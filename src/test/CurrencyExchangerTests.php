<?php


namespace test;


use PHPUnit\Framework\TestCase;
use TransactionProcessor\Classes\CurrencyExchanger;
use TransactionProcessor\Classes\CurrencyRateProvider;

class CurrencyExchangerTests extends TestCase
{
    public function testChange()
    {
        $providerMethod = 'GetCurrencyRate';
        $currency = 'USD';
        $rate = 1.2;
        $amount = 100;
        $expected = $amount / $rate;

        $rateProvider = $this->getMockBuilder(CurrencyRateProvider::class)
            ->setConstructorArgs([ '' , $currency])
            ->setMethods([ $providerMethod ])
            ->getMock();

        $rateProvider->expects($this->once())
            ->method($providerMethod)
            ->will($this->returnValue($rate));

        $exchanger = new CurrencyExchanger($rateProvider);
        $exchangedAmount = $exchanger->Change($amount, $currency);
        $this->assertEquals($expected, $exchangedAmount);
    }
}