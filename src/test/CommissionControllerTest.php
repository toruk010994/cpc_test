<?php


namespace test;


use PHPUnit\Framework\TestCase;
use TransactionProcessor\Classes\CommissionController;
use TransactionProcessor\Classes\CurrencyExchanger;
use TransactionProcessor\Classes\CurrencyRateProvider;
use TransactionProcessor\Classes\EuropeBinRegionValidator;
use TransactionProcessor\Classes\RegionProvider;

class CommissionControllerTest extends TestCase
{
    public function testCalcCommission()
    {
        $currency = 'USD';
        $rate = 1.2;
        $transactionAmount = 200;
        $commission = 0.02;
        $transaction = [ "bin" => 0, "amount" => $transactionAmount , "currency" => $currency ];
        $expected = ceil($transactionAmount / $rate * $commission * 100) / 100;

        $regionProvider = $this->getMockBuilder(RegionProvider::class)
            ->setConstructorArgs([ '' ])
            ->setMethods([ 'GetRegionCodeByCardNumber' ])
            ->getMock();

        $regionProvider->expects($this->once())
            ->method('GetRegionCodeByCardNumber')
            ->willReturn('US');

        $regionValidator = new EuropeBinRegionValidator($regionProvider, [ 'US' ]);

        $rateProvider = $this->getMockBuilder(CurrencyRateProvider::class)
            ->setConstructorArgs([ '' , $currency ])
            ->setMethods([ 'GetCurrencyRate' ])
            ->getMock();

        $rateProvider->expects($this->once())
            ->method('GetCurrencyRate')
            ->will($this->returnValue($rate));

        $exchanger = new CurrencyExchanger($rateProvider);

        $controller = new CommissionController($regionValidator, $exchanger, $commission, 0);
        $result = $controller->CalcCommission($transaction);

        $this->assertEquals($expected, $result);
    }
}