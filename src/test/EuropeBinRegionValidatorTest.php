<?php


namespace test;


use TransactionProcessor\Classes\EuropeBinRegionValidator;
use TransactionProcessor\Classes\RegionProvider;

class EuropeBinRegionValidatorTest extends \PHPUnit\Framework\TestCase
{
    public function testCheckByCode()
    {
        $method = 'GetRegionCodeByCardNumber';
        $regionCodes = [ 'US', 'KZ' ];
        $expected = [ true, false ];
        $allowed = [ 'US' ];


        $regionProvider = $this->getMockBuilder(RegionProvider::class)
            ->setConstructorArgs([ '' ])
            ->setMethods([ $method ])
            ->getMock();

        $regionProvider->expects($this->any())
            ->method($method)
            ->willReturn($regionCodes[0], $regionCodes[1]);

        for ($i = 0; $i < count($regionCodes); $i++) {

            $regionValidator = new EuropeBinRegionValidator($regionProvider, $allowed);
            $checked = $regionValidator->CheckByCode(0);

            $this->assertEquals($expected[$i], $checked);
        }
    }
}