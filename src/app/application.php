<?php
require_once 'vendor/autoload.php';

use TransactionProcessor\Classes\CommissionController;
use TransactionProcessor\Classes\Configuration;
use TransactionProcessor\Classes\CurrencyExchanger;
use TransactionProcessor\Classes\CurrencyRateProvider;
use TransactionProcessor\Classes\EuropeBinRegionValidator;
use TransactionProcessor\Classes\RegionProvider;

try {
    $configPath = realpath("./src/app/config.json");
    $configuration = new Configuration($configPath);

    $regionUrlApi = $configuration->GetRegionValidationUrlApi();
    $regionProvider = new RegionProvider($regionUrlApi);

    $regions = $configuration->GetAllowedRegions();
    $binRegionValidator = new EuropeBinRegionValidator($regionProvider, $regions);

    $exchangerUrlApi = $configuration->GetExchangerUrlApi();
    $currencyName = $configuration->GetExchangerCurrency();
    $currencyRateProvider = new CurrencyRateProvider($exchangerUrlApi, $currencyName);
    $currencyExchanger = new CurrencyExchanger($currencyRateProvider);

    $regionCommission = $configuration->GetCommissionForCurrentRegion();
    $commonCommission = $configuration->GetCommissionForCommonRegion();

    $commissionController = new CommissionController($binRegionValidator, $currencyExchanger, $regionCommission, $commonCommission);

    $path = $argv[1];
    $contents = file_get_contents($path);
    $transactions = json_decode($contents, true);
    if (!$transactions)
    {
        $error = json_last_error_msg();
        throw new Exception("Unable to get transactions info: $error\nGot: $contents");
    }

    foreach ($transactions as $transaction) {
        $commission = $commissionController->CalcCommission($transaction);
        echo "$commission\n";
    }
} catch (Exception $e) {
    $error = $e->getMessage();
    echo "Error occurred while running program:\n$error}";
}

