<?php


namespace TransactionProcessor\Contracts;


interface ICommissionController
{
    function CalcCommission(array $transactionInfo): float;
}