<?php


namespace TransactionProcessor\Contracts;


interface ICurrencyExchanger
{
    function Change(float $amount, string $currency): float;

    function GetCurrentCurrency(): string;
}