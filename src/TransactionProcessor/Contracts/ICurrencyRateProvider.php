<?php


namespace TransactionProcessor\Contracts;


interface ICurrencyRateProvider
{
    function GetCurrencyRate($currencyName): float;

    function GetCurrentCurrency(): string;
}