<?php


namespace TransactionProcessor\Contracts;


interface IBinRegionValidator
{
    function CheckByCode(int $regionCardCode): bool;
}