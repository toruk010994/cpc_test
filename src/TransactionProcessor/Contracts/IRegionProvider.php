<?php


namespace TransactionProcessor\Contracts;


interface IRegionProvider
{
    function GetRegionCodeByCardNumber(int $cardNumber): string;
}