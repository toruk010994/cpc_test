<?php


namespace TransactionProcessor\Classes;


use Exception;
use TransactionProcessor\Contracts\ICurrencyExchanger;
use TransactionProcessor\Contracts\ICurrencyRateProvider;

class CurrencyExchanger implements ICurrencyExchanger
{
    /** @var ICurrencyRateProvider */
    private $currencyRateProvider;

    public function __construct(ICurrencyRateProvider $currencyRateProvider)
    {
        $this->currencyRateProvider = $currencyRateProvider;
    }

    public function GetCurrentCurrency(): string
    {
        return $this->currencyRateProvider->GetCurrentCurrency();
    }

    public function Change(float $amount, string $currency): float
    {
        $rate = $this->currencyRateProvider->GetCurrencyRate($currency);
        $currentCurrency = $this->GetCurrentCurrency();
        if (!strcmp($currentCurrency, $currency) && $rate > 0)
            return $amount / $rate;

        return $amount;
    }
}