<?php


namespace TransactionProcessor\Classes;


use Exception;

class RegionProvider implements \TransactionProcessor\Contracts\IRegionProvider
{
    /** @var string */
    private $urlApi;

    public function __construct(string $urlApi)
    {
        $this->urlApi = $urlApi;
    }

    public function GetRegionCodeByCardNumber(int $cardNumber): string
    {
        $contents = file_get_contents("$this->urlApi/$cardNumber");
        $binlist = json_decode($contents, true);
        if (!$binlist)
        {
            $jsonError = json_last_error_msg();
            throw new Exception("Unable to get bin list: $jsonError.\nGot $contents");
        }

        return $binlist['country']['alpha2'];
    }
}