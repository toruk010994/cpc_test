<?php


namespace TransactionProcessor\Classes;


use Exception;

class Configuration
{
    private $commission = [
        "region" => 0,
        "common" => 0
    ];
    private $exchanger = [
        "currency" => "",
        "urlApi" => ""
    ];
    private $regionValidation = [
        "urlApi" => "",
        "regions" => []
    ];

    public function __construct($path)
    {
        $contents = file_get_contents($path);
        $configuration = json_decode($contents, true);
        if (!$configuration)
        {
            $error = json_last_error_msg();
            throw new Exception("Unable to load configuration from file: $error.");
        }

        $this->commission = $configuration['commission'];
        $this->exchanger = $configuration['exchanger'];
        $this->regionValidation = $configuration['regionValidation'];
    }

    public function GetCommissionForCurrentRegion()
    {
        return $this->commission['region'];
    }

    public function GetCommissionForCommonRegion()
    {
        return $this->commission['common'];
    }

    public function GetExchangerUrlApi()
    {
        return $this->exchanger['urlApi'];
    }

    public function GetExchangerCurrency()
    {
        return $this->exchanger['currency'];
    }

    public function GetRegionValidationUrlApi()
    {
        return $this->regionValidation['urlApi'];
    }

    public function GetAllowedRegions()
    {
        return $this->regionValidation['regions'];
    }
}