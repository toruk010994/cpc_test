<?php


namespace TransactionProcessor\Classes;


use TransactionProcessor\Contracts\IBinRegionValidator;
use TransactionProcessor\Contracts\ICommissionController;
use TransactionProcessor\Contracts\ICurrencyExchanger;

class CommissionController implements ICommissionController
{
    /** @var IBinRegionValidator */
    private $binRegionValidator;
    /** @var ICurrencyExchanger */
    private $currencyExchanger;
    /** @var float */
    private $regionCommission;
    /** @var float */
    private $commonCommission;

    public function __construct(
        IBinRegionValidator $binRegionValidator,
        ICurrencyExchanger $currencyExchanger,
        float $regionCommission,
        float $commonCommission)
    {
        $this->binRegionValidator = $binRegionValidator;
        $this->currencyExchanger = $currencyExchanger;
        $this->regionCommission = $regionCommission;
        $this->commonCommission = $commonCommission;
    }

    public function CalcCommission(array $transactionInfo): float
    {
        $binNumber = $transactionInfo['bin'];
        $amount = $transactionInfo['amount'];
        $currency = $transactionInfo['currency'];

        $commission = $this->binRegionValidator->CheckByCode($binNumber) ?
            $this->regionCommission :
            $this->commonCommission;

        $exchangedAmount = $this->currencyExchanger->Change($amount, $currency);
        $commissionAmount = $exchangedAmount * $commission;

        return ceil($commissionAmount * 100) / 100;
    }
}