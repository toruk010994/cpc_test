<?php


namespace TransactionProcessor\Classes;


use Exception;
use TransactionProcessor\Contracts\ICurrencyRateProvider;

class CurrencyRateProvider implements ICurrencyRateProvider
{
    /** @var string */
    private $urlApi;
    /**  @var string */
    private $currency;

    public function __construct(string $urlApi, string $currencyName)
    {
        $this->urlApi = $urlApi;
        $this->currency = $currencyName;
    }

    public function GetCurrencyRate($currencyName): float
    {
        $contents = file_get_contents($this->urlApi);
        $exchangeInfo = json_decode($contents, true);
        if (!$exchangeInfo)
        {
            $error = json_last_error_msg();
            throw new Exception("Unable to load exchanger info: $error.\nGot: $contents");
        }

        $rate = $exchangeInfo['rates'][$this->currency];

        return $rate ? $rate : 0;
    }

    function GetCurrentCurrency(): string
    {
        return $this->currency;
    }
}