<?php


namespace TransactionProcessor\Classes;


use Exception;
use TransactionProcessor\Contracts\IBinRegionValidator;
use TransactionProcessor\Contracts\IRegionProvider;

class EuropeBinRegionValidator implements IBinRegionValidator
{
    /** @var IRegionProvider */
    private $regionProvider;
    /** @var array */
    private $allowedRegionCodes;

    public function __construct(IRegionProvider $regionProvider, array $allowedRegionCodes)
    {
        $this->regionProvider = $regionProvider;
        $this->allowedRegionCodes = $allowedRegionCodes;
    }

    function CheckByCode(int $regionCardCode): bool
    {
        $regionCode = $this->regionProvider->GetRegionCodeByCardNumber($regionCardCode);

        return $this->isEu($regionCode);
    }

    function isEu($region): bool
    {
        return in_array($region, $this->allowedRegionCodes);
    }
}